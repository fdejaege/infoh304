#include <set>
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

void printSet(const set<string> & c, ostream & out = cout)
{
	set<string>::const_iterator itr;
	for ( itr = c.begin(); itr !=c.end(); ++itr)
		out << *itr << endl;
}


int main(int argc, char *argv[])
{
	if (argc ==1)
	{
		cout << "Veuillez inclure un nom de fichier" << endl;
	}
	else
	{
		ifstream fichier (argv[1]);
		if (fichier.is_open())
		{
			cout << "Fichier ouvert" << endl;
			set<string> liste;
			string mot;
			while (fichier >> mot)
				{
					liste.insert(mot);
				}
			fichier.close();
			printSet(liste);
			cout << "Nombre de mots uniques : " << liste.size() << endl;
		}
		
		else
			cout << "Impossible d'ouvrir le fichier" << endl;
	
	
	
	}
	return 0;
}
